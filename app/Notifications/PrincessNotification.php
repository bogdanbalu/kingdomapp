<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PrincessNotification extends Notification
{
    use Queueable;

    public $princessName;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($princessName)
    {
        $this->precessName = $princessName;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Hello princess '.$this->precessName)
                    ->line('There are three knights in the kingdom, suitors to your hand')
                    ->line('After pressing the below button, you will see a list of them. Please remove one that you think is not for you')
                    ->action('Go to list', url('http://localhost:8000/player/knights/remaining/list'))
                    ->line('Thank you princess '.$this->precessName.'!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
