<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Battle extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $casts = [
        'knight_id' => 'integer',
        'damage' => 'integer',
        'health' => 'integer',
        'attack' => 'integer',
    ];
}
