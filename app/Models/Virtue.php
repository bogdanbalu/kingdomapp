<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Virtue extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }

    public function setKeyAttribute($value)
    {
        $this->attributes['key'] = strtolower($value);
    }

    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }
}
