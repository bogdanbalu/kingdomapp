<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KnightVirtue extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $casts = [
        'knight_id' => 'integer',
        'virtue_id' => 'integer',
        'score' => 'integer'
    ];
}
