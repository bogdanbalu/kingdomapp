<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Knight extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $casts = [
        'user_id' => 'integer',
        'princess_id' => 'integer',
        'winner' => 'boolean',
        'age' => 'integer',
        'virtue_avarage' => 'integer'
    ];

    public function setNameAttribute($value) {
        $this->attributes['name'] = strtolower($value);
    }

    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    public function knightVirtues() {
        return $this->hasMany(KnightVirtue::class);
    }

    public function knightSkills() {
        return $this->hasMany(KnightsSkills::class);
    }

    public function battles() {
        return $this->hasOne(Battle::class);
    }

}
