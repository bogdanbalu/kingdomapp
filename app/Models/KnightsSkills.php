<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KnightsSkills extends Model
{
    use HasFactory;

    const STRENGHT = 1;
    const DEFENSE = 2;
    const BATTLE_STRATEGY = 3;

    protected $guarded = ['id'];

    protected $casts = [
        'knight_id' => 'integer',
        'skill_id' => 'integer',
        'score' => 'integer'
    ];

    public function skill() {
        return $this->HasMany(Skill::class, 'id', 'skill_id');
    }
}
