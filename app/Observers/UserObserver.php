<?php

namespace App\Observers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Knight;
use App\Models\Princess;
use App\Http\Traits\KnightTrait;
use Illuminate\Support\Facades\DB;

class UserObserver
{
    use KnightTrait;
    /**
     * Handle the User "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(User $user)
    {
        $princess = $this->createPrincess($user->id);
        $this->createKnights($user->id, $princess->id);

        $this->generateRandomKnightVitues($user->id);
        $this->generateRandomKnightSkills($user->id);
    }
}
