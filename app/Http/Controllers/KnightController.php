<?php

namespace App\Http\Controllers;

use App\Models\Knight;
use Illuminate\Http\Request;
use App\Http\Traits\KnightTrait;
use App\Http\Resources\KnightResource;
use App\Models\Battle;
use Carbon\Carbon;

class KnightController extends Controller
{
    use KnightTrait;

    /**
     * Show page and all knights of the game
     *
     * @return void
     */
    public function showKnightsList() {
        $knights = $this->getKnights(auth()->id());
        $count = count($knights->whereNotNull('virtue_avarage'));
        return view('player.knights_list', ['knights' => KnightResource::collection($knights), 'count' => $count]);
    }

    /**
     * Page and three knights 
     *
     * @return void
     */
    public function showKnithsForPrincess() {
        $knights = Knight::with('knightVirtues')->whereNotNull('princess_id')->get();
        foreach ($knights as $knight) {
            $knight->update(['virtue_avarage' => round($knight['knightVirtues']->avg('score'))]);
        }
        $knights = collect($knights)->sortByDesc('virtue_avarage');
        $remainingKnights = $knights->take(3); 
        $knightsIds = $knights->skip(3)->pluck('id');
        Knight::whereIn('id', $knightsIds)->update(['princess_id' => null]);
        $countPrincess = $remainingKnights->pluck('princess_id')->count();
        return view('player.knights_princess', ['knights' => KnightResource::collection($remainingKnights), 'countPrincess' => $countPrincess]);
    }

    /**
     * Remove knight of princess
     *
     * @param Knight $knight
     * @return void
     */
    public function removeKnight(Knight $knight) {
        $result = [];
        $knight->update(['princess_id' => null]);
        $knights = KnightTrait::getKnights(auth()->id())->whereNotNull('princess_id');
        collect($knights)->map(function ($knight) use(&$result) {
            $result[] = [
                'knight_id' => $knight->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        });
        Battle::insert($result);
        return back();
    }
}
