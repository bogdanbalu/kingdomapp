<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Battle;
use App\Models\Knight;
use App\Models\Princess;
use App\Models\KnightsSkills;
use App\Http\Traits\KnightTrait;
use App\Notifications\PrincessNotification;
use Illuminate\Support\Facades\Notification;

class BattleController extends Controller
{
    use KnightTrait;
    /**
     * Show the page and the knights witch will fight
     *
     * @return void
     */
    public function showBattle() {
        $knights = Knight::where('user_id', auth()->id())->whereNotNull('princess_id')
            ->join('battles', 'knights.id', 'battles.knight_id')
            ->select('knights.id', 'knights.name', 'battles.damage', 'battles.health', 'battles.attack')
            ->get();
        return view('player.battle', ['knights' => $knights]);
    }

    /**
     * This is the action of each knight
     *
     * @param Knight $knight
     * @return void
     */
    public function attack(Knight $knight) {
        $opponent = Knight::where('user_id', auth()->id())->whereNotNull('princess_id')->where('id', '<>', $knight->id)->first();
        $skills = KnightsSkills::where('knight_id', $knight->id)->select('score')->get()->toArray();
        $totalAttacks = Battle::whereIn('knight_id', [$knight->id, $opponent->id])->pluck('attack')->sum();
        $strength = $skills[0]['score'];
        $defense = $skills[1]['score'];
        $battleStrategy = $skills[2]['score'];
        $knightBattles = $knight->battles()->latest()->first();
        $opponentBattles = $opponent->battles()->latest()->first();
        if ($knightBattles->attack === 0 && $opponentBattles->attack === 0) {
            $opponentStrategy = $opponent->knightSkills()->where('skill_id', KnightsSkills::BATTLE_STRATEGY)->first()->score;
            if ($battleStrategy === $opponentStrategy) {
                $opponentStrenght = $opponent->knightSkills()->where('skill_id', KnightsSkills::STRENGHT)->first()->score;
                if ($strength < $opponentStrenght) {
                    return redirect('/player/battle')->with(['error' => 'You can not attack. Your opponent need to attack']);
                }
                $damage = $this->damage($strength, $battleStrategy, $defense);
                $this->updateBattle($knight->id, $damage, $knightBattles->health, $opponent->id, $opponentBattles->damage, $opponentBattles->health-$damage);
                return back();
            } 
            if ($battleStrategy < $opponentStrategy) {
                return redirect('/player/battle')->with(['error' => 'You can not attack. Your opponent need to attack']);
            }
            $damage = $this->damage($strength, $battleStrategy, $defense);
            $this->updateBattle($knight->id, $damage, $knightBattles->health, $opponent->id, $opponentBattles->damage, $opponentBattles->health-$damage);
            return back();
        }
        
        if ($knightBattles->attack > $opponentBattles->attack) {
            return redirect('/player/battle')->with(['error' => 'You can not attack. Your opponent need to attack']);
        }

        if ($knightBattles->health < 20 || $opponentBattles->health < 20 || $totalAttacks >= 4) {
            if ($knightBattles->health > $opponentBattles->health) {
                $knight->update(['winner' => 1]);
                return redirect('/player/battle')->with(['success' => 'The game is over '.$knightBattles->name.' win']);
            } elseif ($knightBattles->health < $opponentBattles->health) {
                $opponent->update(['winner' => 1]);
                return redirect('/player/battle')->with(['success' => 'The game is over '.$opponent->name.' win']);
            } 
            return redirect('/player/battle')->with(['info' => 'Draw']);
        }
        $damage = $this->damage($strength, $battleStrategy, $defense);
        $this->updateBattle($knight->id, $damage, $knightBattles->health, $opponent->id, $opponentBattles->damage, $opponentBattles->health-$damage);
        return back();
    }
/**
 * Calculate damage
 *
 * @param integer $strenght
 * @param integer $battleStrategy
 * @param integer $defense
 */
    public function damage(int $strenght, int $battleStrategy, int $defense) {
        return $strenght + ($strenght * $battleStrategy)/100 - ($defense);
    }

    /**
     * Create the new registers of the battle
     *
     * @param integer $knightId
     * @param integer $damge
     * @param integer $lastHealth
     * @param integer $opponentId
     * @param integer $opponentDamage
     * @param integer $oppoentHealth
     */
    public function updateBattle(int $knightId, int $damge, int $lastHealth, int $opponentId, int $opponentDamage, int $oppoentHealth) {
        $result = [
            [
                'knight_id' => $knightId,
                'damage' => $damge,
                'health' => $lastHealth,
                'attack' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'knight_id' => $opponentId,
                'damage' => $opponentDamage,
                'health' => $oppoentHealth,
                'attack' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];
        return Battle::insert($result);
    }

    /**
     * Create a new game
     *
     * @return void
     */
    public function newGame() {
        $winner = Knight::where('user_id', auth()->id())->where('winner', 1)->first();
        $princess = Princess::where('user_id', auth()->id())->first();
        if (!$winner) {
            return redirect('/home')->with(['error' => 'Please finish the current game']);
        }
        Knight::where('user_id', auth()->id())->delete();
        $this->createKnights(auth()->id(), $princess->id);
        $this->generateRandomKnightVitues(auth()->id());
        $this->generateRandomKnightSkills(auth()->id());
        Notification::send(auth()->user(), new PrincessNotification($princess->name));
        return redirect('/home')->with(['success' => 'The new game has begun']);
    }
}
