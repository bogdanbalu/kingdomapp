<?php

namespace App\Http\Controllers;

use App\Models\Knight;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $winner = Knight::where('user_id', auth()->id())->where('winner', 1)->first();
        return view('player.home', ['winner' => $winner]);
    }
}
