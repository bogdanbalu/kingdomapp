<?php

namespace App\Http\Traits;

use App\Models\User;
use App\Models\Skill;
use App\Models\Knight;
use App\Models\Virtue;
use App\Models\Princess;
use App\Models\KnightVirtue;
use App\Models\KnightsSkills;
use Illuminate\Support\Carbon;
use App\Notifications\PrincessNotification;
use Illuminate\Support\Facades\Notification;

trait KnightTrait {

    /**
     * Get Knights by user id
     *
     * @param integer $userId
     */
    public function getKnights(int $userId) {
        return Knight::where('user_id', $userId)->get();
    }
/**
 * Generate random knight virtues scores
 *
 * @param integer $userId
 */
    public function generateRandomKnightVitues(int $userId) {
        $result = [];
        $knights = $this->getKnights($userId);
        $quantity = $knights->count();
        $virtues = Virtue::get();
        foreach ($virtues as $virtue) {
            if (in_array($virtue->key, config('virtue.older'))) {
                $knights = collect($knights)->sortByDesc('age');
                $score = $this->uniqueRandomNumbersWithinRange(0, 100, $quantity, 'desc');
            } elseif (in_array($virtue->key, config('virtue.younger'))) {
                $knights = collect($knights)->sortBy('age');
                $score = $this->uniqueRandomNumbersWithinRange(0, 100, $quantity, 'asc');
            } else {
                $knights = collect($knights);
                $score = $this->uniqueRandomNumbersWithinRange(0, 100, $quantity);
            }
            $knightScores = array_combine($knights->pluck('id')->toArray(), $score);
            $knights->map(function ($knight) use (&$result, $virtue, $knightScores) {
                $result[] = [
                    'knight_id' => $knight->id,
                    'virtue_id' => $virtue->id,
                    'score' => $knightScores[$knight->id],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            });
        }
        return KnightVirtue::insert($result);
    }

    /**
     * Generate random knights skills
     *
     * @param integer $userId
     */
    public function generateRandomKnightSkills(int $userId) {
        $result = [];
        $knights = $this->getKnights($userId);
        $quantity = $knights->count();
        $skills = Skill::get();
        foreach ($skills as $skill) {
            if ($skill->key === 'strength') {
                $knights = collect($knights)->sortByDesc('age');
                $score = $this->uniqueRandomNumbersWithinRange(60, 100, $quantity, 'desc');
            } elseif ($skill->key === 'defense') {
                $knights = collect($knights)->sortBy('age');
                $score = $this->uniqueRandomNumbersWithinRange(20, 60, $quantity, 'asc');
            } else {
                $knights = collect($knights);
                $score = $this->uniqueRandomNumbersWithinRange(20, 40, $quantity);
            }
            $knightScores = array_combine($knights->pluck('id')->toArray(), $score);
            $knights->map(function ($knight) use (&$result, $skill, $knightScores) {
                $result[] = [
                    'knight_id' => $knight->id,
                    'skill_id' => $skill->id,
                    'score' => $knightScores[$knight->id],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            });
        }
        return KnightsSkills::insert($result);
    }
    /**
     * Generate random virtues score
     *
     * @param [int] $min
     * @param [int] $max
     * @param [int] $quantity
     * @param [string] $order
     */
    private function uniqueRandomNumbersWithinRange(int $min, int $max, int $quantity, string $order = null) {
        $numbers = range($min, $max);
        shuffle($numbers);
        $data = collect(array_slice($numbers, 0, $quantity));
        if ($order === 'asc') {
            return $data->sort()->values()->all();
        } elseif ($order === 'desc') {
            return $data->sortDesc()->values()->all();
        }
        return $data->values()->all();
    }

    /**
     * Create Knights
     *
     * @param integer $userId
     * @param integer $princessId
     */
    public function createKnights(int $userId, int $princessId) {
        $knights = [
            [
                'user_id' => $userId,
                'princess_id' => $princessId,
                'name' => 'Kennith Bogisich',
                'age' => rand(20, 25),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => $userId,
                'princess_id' => $princessId,
                'name' => 'Llewellyn Wolf',
                'age' => rand(20, 25),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => $userId,
                'princess_id' => $princessId,
                'name' => 'Stephen Lueilwitz',
                'age' => rand(20, 25),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => $userId,
                'princess_id' => $princessId,
                'name' => 'Paxton Block',
                'age' => rand(20, 25),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => $userId,
                'princess_id' => $princessId,
                'name' => 'Luther Macejkovic',
                'age' => rand(20, 25),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];
        return $knights = Knight::insert($knights);
    }

   /**
    * Create Princess
    *
    * @param integer $userId
    */
    public function createPrincess(int $userId) {
        $user = User::find($userId);
        $princess = Princess::create([
            'user_id' => $userId,
            'name' => 'Kelsie Kirlin'
        ]);
        Notification::send($user, new PrincessNotification($princess->name));
        return $princess;
    }
}