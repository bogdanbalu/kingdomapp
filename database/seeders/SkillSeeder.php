<?php

namespace Database\Seeders;

use App\Models\Skill;
use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Skill::insert([
            [
                'id' => 1,
                'name' => 'Strength',
                'key' => 'strength'
            ],
            [
                'id' => 2,
                'name' => 'Defense',
                'key' => 'defense'
            ],
            [
                'id' => 3,
                'name' => 'Battle strategy',
                'key' => 'battle_strategy'
            ],
        ]);
    }
}
