<?php

namespace Database\Seeders;

use App\Models\Virtue;
use Illuminate\Database\Seeder;

class VirtueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Virtue::insert([
            [
                'id' => 1,
                'name' => 'Courage',
                'key' => 'courage'
            ],
            [
                'id' => 2,
                'name' => 'Justice',
                'key' => 'justice'
            ],
            [
                'id' => 3,
                'name' => 'Mercy',
                'key' => 'mercy'
            ],
            [
                'id' => 4,
                'name' => 'Generosity',
                'key' => 'generosity'
            ],
            [
                'id' => 5,
                'name' => 'Faith',
                'key' => 'faith'
            ],
            [
                'id' => 6,
                'name' => 'Nobility',
                'key' => 'nobility'
            ],
            [
                'id' => 7,
                'name' => 'Hope',
                'key' => 'hope'
            ]
        ]);
    }
}
