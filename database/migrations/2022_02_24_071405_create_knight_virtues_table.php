<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKnightVirtuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knight_virtues', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('knight_id');
            $table->unsignedBigInteger('virtue_id');
            $table->tinyInteger('score');
            $table->timestamps();

            $table->foreign('knight_id')->references('id')->on('knights')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('virtue_id')->references('id')->on('virtues')->onDelete('RESTRICT')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('knight_virtues');
    }
}
