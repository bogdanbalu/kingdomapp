<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKnightsSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knights_skills', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('knight_id');
            $table->unsignedBigInteger('skill_id');
            $table->tinyInteger('score');
            $table->timestamps();

            $table->foreign('knight_id')->references('id')->on('knights')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('skill_id')->references('id')->on('skills')->onDelete('RESTRICT')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('knights_skills');
    }
}
