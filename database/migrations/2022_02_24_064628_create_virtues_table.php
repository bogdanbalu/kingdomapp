<?php

use Database\Seeders\VirtueSeeder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVirtuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('virtues', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('key');
        });
        $virtues = new VirtueSeeder();
        $virtues->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('virtues');
    }
}
