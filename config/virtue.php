<?php 

return [
    'older' => ['courage', 'justice', 'faith'],
    'younger' => ['mercy', 'generosity', 'hope']
];