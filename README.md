## Installation

First clone this repository, install the dependencies, and setup your .env file.

```
git clone git@bitbucket.org:bogdanbalu/kingdomapp.git
composer install
cp .env.example .env
```

Run the initial migrations and seeders.

```
php artisan migrate

## Specifications

First create an account. After creating the account, will be generated five knights, a princess, random vitues and skills for each knights.
After it will be calculated avarage vitues for each knights and sort desc. The princess will receive a mail with first three knights and she will need to remove one knight. For mail i use a testing tool. After the knight was removed, the battle can start. 

 Front end:
 In user menu exists:
 New game button -> can start a new game after the last game is over
 Winner -> Will display the winner
 Knights -> A list with all user knights
 Battle -> Will display a table with those two knights and with attack buttons