@extends('layouts.app')

@section('content')
<script src="{{ asset('js/player.js') }}" type="text/javascript"></script>
<link href="{{ asset('css/player.css') }}" rel="stylesheet">
<div class="container">
    @include('player.sidebar')
    <div class="row justify-content-center">
        @if (Session::has('error'))
            <div id="alert" class="alert alert-danger title" role="alert">
            {!! session('error') !!}
            </div>
        @endif
        @if (Session::has('success'))
            <div id="alert" class="alert alert-success title" role="alert">
            {!! session('success') !!}
            </div>
        @endif
        <div class="col-md-8">
            <div class="card">
                <div class="card-body title">
                    @if ($winner <> null)
                        <h3>Congratulations {{ $winner->name }} you win!</h3></br>
                        <img class="resize" src="{{url('/images/knight.png')}}" alt="Image"/>
                    @endif
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
