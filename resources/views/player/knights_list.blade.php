@extends('layouts.app')
@section('content')
<link href="{{ asset('css/player.css') }}" rel="stylesheet">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                @include('player.sidebar')
            </div>
            <div class="col-sm-10">
                <h3 class="title">Knights</h3>
                <table class="table center">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Picture</th>
                            <th scope="col">Name</th>
                            <th scope="col">Age</th>
                            @if($count > 0)
                                <th scope="col">Virtue Average Score</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($knights as $key => $knight)
                            <tr>
                                <th>{{ $loop->iteration }}</th>
                                <td><img class="resize-knight-list" src="{{url('/images/knight.png')}}" alt="Image"/></td>
                                <td>{{ $knight->name }}</td>
                                <td>{{ $knight->age }}</td>
                                @if($count > 0)
                                    <td>{{ $knight->virtue_avarage }}</td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection