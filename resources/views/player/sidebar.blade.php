<link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
<script src="{{ asset('js/player.js') }}" type="text/javascript"></script>
<div class="wrapper">
    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3 class="title">Menu</h3>
        </div>

        <ul class="list-unstyled components">
            <li class="active">
                <li>
                    <a href="{{ url('player/new-game') }}" class="text">New Game</a>
                </li>
                <li>
                    <a href="{{ url('home') }}" class="text">Winner</a>
                </li>
                <li>
                    <a href="{{ url('player/knights/list') }}" class="text">Knights</a>
                </li>
                <li>
                    <a href="{{ url('player/battle') }}" class="text" class="text">Battle</a>
                </li>
            </li>
        </ul>
    </nav>
</div>