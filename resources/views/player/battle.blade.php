@extends('layouts.app')
@section('content')
<link href="{{ asset('css/player.css') }}" rel="stylesheet">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                @include('player.sidebar')
            </div>
            <div class="col-sm-10">
                <h3 class="title">Battle</h3>
                @if (Session::has('error'))
                    <div id="alert" class="alert alert-danger title" role="alert">
                    {!! session('error') !!}
                    </div>
                @endif
                @if (Session::has('success'))
                    <div id="alert" class="alert alert-success title" role="alert">
                    {!! session('success') !!}
                    </div>
                @endif
                @if (Session::has('info'))
                    <div id="alert" class="alert alert-info title" role="alert">
                    {!! session('info') !!}
                    </div>
                @endif
                <table class="table center table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Knight</th>
                            <th scope="col">Damage</th>
                            <th scope="col">Health</th>
                            <th scope="col">Attack</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($knights as $knight)
                            <tr>
                                <td>{{ $knight['name'] }}</td>
                                <td>{{ $knight['damage'] }}</td>
                                <td>{{ $knight['health'] }}</td>
                                <td>{{ $knight['attack'] }}</td>
                                <td>
                                    <div class="row">
                                        <form method="post" action="{{ url('/player/battle/attack', $knight['id']) }}">
                                        @csrf
                                            <button type="submit" class="btn btn-sm edit-button" style="color: white" data-toggle="tooltip" data-placement="top">Attack</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection