<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BattleController;
use App\Http\Controllers\KnightController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'showLoginForm']);

Auth::routes();
Route::group(['middleware' => ['auth']], function() {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::prefix('player')->group(function() {
        Route::get('knights/list', [KnightController::class, 'showKnightsList']);
        Route::get('knights/remaining/list', [KnightController::class, 'showKnithsForPrincess']);
        Route::post('remove/knight/{knight}', [KnightController::class, 'removeKnight'])->name('remove');
        Route::get('new-game', [BattleController::class, 'newGame']);
        Route::prefix('battle')->group(function() {
            Route::get('/', [BattleController::class, 'showBattle']);
            Route::post('/attack/{knight}', [BattleController::class, 'attack'])->name('attack');
        });
    });
});

